import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const ProductRepository = AppDataSource.getRepository(Product)


    const products = await ProductRepository.find()
    console.log("Loaded products: ", products)

    const updateProduct = await ProductRepository.findOneBy({id:1})
    console.log(updateProduct)
    updateProduct.price=80
    await ProductRepository.save(updateProduct)

}).catch(error => console.log(error))
